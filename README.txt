﻿

	 +------+       +------+       +------+       +------+       +------+
	 |`.    |`.     |\     |\      |      |      /|     /|     .'|    .'|
	 |  `+--+---+   | +----+-+     +------+     +-+----+ |   +---+--+'  |
	 |   |  |   |   | |    | |     |      |     | |    | |   |   |  |   |
	 +---+--+   |   +-+----+ |     +------+     | +----+-+   |   +--+---+
	  `. |   `. |    \|     \|     |      |     |/     |/    | .'   | .'
		`+------+     +------+     +------+     +------+     +------+'
	B			  O				G			 G			  L			    E
		.+------+     +------+     +------+     +------+     +------+.
	  .' |    .'|    /|     /|     |      |     |\     |\    |`.    | `.
	 +---+--+'  |   +-+----+ |     +------+     | +----+-+   |  `+--+---+
	 |   |  |   |   | |    | |     |      |     | |    | |   |   |  |   |
	 |  ,+--+---+   | +----+-+     +------+     +-+----+ |   +---+--+   |
	 |.'    | .'    |/     |/      |      |      \|     \|    `. |   `. |
	 +------+'      +------+       +------+       +------+      `+------+
(ascii art by Richard Kirk http://www.chris.com/ascii/index.php?art=objects/boxes)

!!!IMPORTANT FOR RUNNING TESTS:
		You must click OK on all of the popup dialogs when they pop up stating that the opponent left,
		server crashed and You gave up for the tests to finish correctly! The server window will also 
		not close on its own. Once the test has passed, you may close the server instance. The tests
		sometimes require a couple of runs to "warm up". They may not pass the first time through.

////// This BoggleClient implementation was created by:
		Scott Wells and Matthew Mendez
		For Fall2014 CS3500 at the University of Utah 
		Date:	December 5th, 2014

////// HOW TO USE:
	- Open the client
	- Toggle the Connect/Disconnect by clicking the button in the menu bar.
	- Enter the server IP or domain name in the ServerIP text field
	- Enter your name in the PlayerName text field
	- Press the GO! button 
		Note: The GO! button attempts to establish a connection to the server. This may take
			  a moment, It may appear that the client is not responding once you hit go, but
			  it will eventually display a status message notifying you if it was successful
			  in the Summary text box.

	- Once an opponent has entered the game with you, Their name will be displayed on the board
		and say "[Name]'s Score:" underneath "Your Score:"

	- During the game:
		- Enter words into the text box directly below the board to play them. You may submit the
		  word by clicking the Attack! button or by simply pressing enter. 
	
		- Another optional way of entering the word you want to play is by clicking the boggle board 
		  letters themselves. It will add the letter clicked to the word submission box. You
		  may still press enter or click Attack! to submit this way as well.

		- You may disconnect from the game at any time by toggling the Connect/Disconnect menu item.
		  The GO! button is now replaced with a Disconnect button to leave the server.

	- Once the game finishes:
		- The client will give you the final score and tell you if you won or not.
		- A summary of the match will be printed to the Summary text box.
		- You may play again by clicking the GO! button (if you want to play on the same server)
		  or you may enter in a different domain or IP to connect to a different server for the 
		  next game.

////// VARIOUS IMPLEMENTATION NOTES:
		While designing the overall look of the gui, we had a discussion about what the best 
	and most natural format would be. We decided that instead of going with an introductory
	splash screen asking for the IP and Name, that we would have it in a submenu. The design
	of text input box also allows for the ENTER key to click the Attack! button. This feels
	very natural and is an easy add-in to make it so you don't have to move from typing to
	using the mouse to submit the word during a game.
		
		TESTING ISSUES: We decided to make all the gui event handlers public to be tested in
	the test project. There was an issue with a PrivateObject not being able to invoke any
	private event handling events in the gui controls. Although this could definitely be
	seen as bad practice, we could not find any way around it to get the proper test coverage
	needed by the assignment.