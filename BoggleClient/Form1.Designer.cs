﻿namespace BoggleClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.connectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.connectPanel = new System.Windows.Forms.Panel();
            this.connectDisconnect = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nameField = new System.Windows.Forms.TextBox();
            this.IPField = new System.Windows.Forms.TextBox();
            this.WordTextBox = new System.Windows.Forms.TextBox();
            this.AttackButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.TimeLeftLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.OpponentNameLabel = new System.Windows.Forms.Label();
            this.OpponentsScoreLabel = new System.Windows.Forms.Label();
            this.ClientScoreLabel = new System.Windows.Forms.Label();
            this.SummaryTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.GameOutcomeLabel = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.connectPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(683, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // connectToolStripMenuItem
            // 
            this.connectToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.connectToolStripMenuItem.Name = "connectToolStripMenuItem";
            this.connectToolStripMenuItem.Size = new System.Drawing.Size(128, 20);
            this.connectToolStripMenuItem.Text = "Connect/Disconnect";
            this.connectToolStripMenuItem.Click += new System.EventHandler(this.connectToolStripMenuItem_Click_1);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button16);
            this.panel1.Controls.Add(this.button15);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.button13);
            this.panel1.Controls.Add(this.button12);
            this.panel1.Controls.Add(this.button11);
            this.panel1.Controls.Add(this.button10);
            this.panel1.Controls.Add(this.button9);
            this.panel1.Controls.Add(this.button8);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Location = new System.Drawing.Point(13, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(338, 308);
            this.panel1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.PapayaWhip;
            this.button1.Location = new System.Drawing.Point(3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(77, 70);
            this.button1.TabIndex = 0;
            this.button1.Text = "W";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.BoardButton_Click);
            this.button1.MouseEnter += new System.EventHandler(this.ChangeColorMouseEnter);
            this.button1.MouseLeave += new System.EventHandler(this.ChangeColorMouseLeave);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button16.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button16.FlatAppearance.BorderSize = 0;
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.ForeColor = System.Drawing.Color.PapayaWhip;
            this.button16.Location = new System.Drawing.Point(258, 235);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(77, 70);
            this.button16.TabIndex = 15;
            this.button16.Text = "!";
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.BoardButton_Click);
            this.button16.MouseEnter += new System.EventHandler(this.ChangeColorMouseEnter);
            this.button16.MouseLeave += new System.EventHandler(this.ChangeColorMouseLeave);
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button15.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button15.FlatAppearance.BorderSize = 0;
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.ForeColor = System.Drawing.Color.PapayaWhip;
            this.button15.Location = new System.Drawing.Point(170, 235);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(82, 70);
            this.button15.TabIndex = 14;
            this.button15.Text = "E";
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.BoardButton_Click);
            this.button15.MouseEnter += new System.EventHandler(this.ChangeColorMouseEnter);
            this.button15.MouseLeave += new System.EventHandler(this.ChangeColorMouseLeave);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button14.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.ForeColor = System.Drawing.Color.PapayaWhip;
            this.button14.Location = new System.Drawing.Point(86, 235);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(78, 70);
            this.button14.TabIndex = 13;
            this.button14.Text = "L";
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.BoardButton_Click);
            this.button14.MouseEnter += new System.EventHandler(this.ChangeColorMouseEnter);
            this.button14.MouseLeave += new System.EventHandler(this.ChangeColorMouseLeave);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button13.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button13.FlatAppearance.BorderSize = 0;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.Color.PapayaWhip;
            this.button13.Location = new System.Drawing.Point(3, 235);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(77, 70);
            this.button13.TabIndex = 12;
            this.button13.Text = "G";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.BoardButton_Click);
            this.button13.MouseEnter += new System.EventHandler(this.ChangeColorMouseEnter);
            this.button13.MouseLeave += new System.EventHandler(this.ChangeColorMouseLeave);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button12.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.PapayaWhip;
            this.button12.Location = new System.Drawing.Point(258, 156);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(77, 70);
            this.button12.TabIndex = 11;
            this.button12.Text = "G";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.BoardButton_Click);
            this.button12.MouseEnter += new System.EventHandler(this.ChangeColorMouseEnter);
            this.button12.MouseLeave += new System.EventHandler(this.ChangeColorMouseLeave);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button11.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.Color.PapayaWhip;
            this.button11.Location = new System.Drawing.Point(170, 156);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(82, 70);
            this.button11.TabIndex = 10;
            this.button11.Text = "O";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.BoardButton_Click);
            this.button11.MouseEnter += new System.EventHandler(this.ChangeColorMouseEnter);
            this.button11.MouseLeave += new System.EventHandler(this.ChangeColorMouseLeave);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button10.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ForeColor = System.Drawing.Color.PapayaWhip;
            this.button10.Location = new System.Drawing.Point(86, 156);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(78, 70);
            this.button10.TabIndex = 9;
            this.button10.Text = "B";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.BoardButton_Click);
            this.button10.MouseEnter += new System.EventHandler(this.ChangeColorMouseEnter);
            this.button10.MouseLeave += new System.EventHandler(this.ChangeColorMouseLeave);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button9.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.Color.PapayaWhip;
            this.button9.Location = new System.Drawing.Point(3, 155);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(77, 70);
            this.button9.TabIndex = 8;
            this.button9.Text = "O";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.BoardButton_Click);
            this.button9.MouseEnter += new System.EventHandler(this.ChangeColorMouseEnter);
            this.button9.MouseLeave += new System.EventHandler(this.ChangeColorMouseLeave);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button8.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.Color.PapayaWhip;
            this.button8.Location = new System.Drawing.Point(258, 80);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(77, 70);
            this.button8.TabIndex = 7;
            this.button8.Text = "T";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.BoardButton_Click);
            this.button8.MouseEnter += new System.EventHandler(this.ChangeColorMouseEnter);
            this.button8.MouseLeave += new System.EventHandler(this.ChangeColorMouseLeave);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button7.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.PapayaWhip;
            this.button7.Location = new System.Drawing.Point(170, 80);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(82, 70);
            this.button7.TabIndex = 6;
            this.button7.Text = "E";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.BoardButton_Click);
            this.button7.MouseEnter += new System.EventHandler(this.ChangeColorMouseEnter);
            this.button7.MouseLeave += new System.EventHandler(this.ChangeColorMouseLeave);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.PapayaWhip;
            this.button6.Location = new System.Drawing.Point(86, 80);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(78, 70);
            this.button6.TabIndex = 5;
            this.button6.Text = "M";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.BoardButton_Click);
            this.button6.MouseEnter += new System.EventHandler(this.ChangeColorMouseEnter);
            this.button6.MouseLeave += new System.EventHandler(this.ChangeColorMouseLeave);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.PapayaWhip;
            this.button5.Location = new System.Drawing.Point(3, 79);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(77, 70);
            this.button5.TabIndex = 4;
            this.button5.Text = "O";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.BoardButton_Click);
            this.button5.MouseEnter += new System.EventHandler(this.ChangeColorMouseEnter);
            this.button5.MouseLeave += new System.EventHandler(this.ChangeColorMouseLeave);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.PapayaWhip;
            this.button4.Location = new System.Drawing.Point(258, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(77, 70);
            this.button4.TabIndex = 3;
            this.button4.Text = "C";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.BoardButton_Click);
            this.button4.MouseEnter += new System.EventHandler(this.ChangeColorMouseEnter);
            this.button4.MouseLeave += new System.EventHandler(this.ChangeColorMouseLeave);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.PapayaWhip;
            this.button3.Location = new System.Drawing.Point(170, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(82, 70);
            this.button3.TabIndex = 2;
            this.button3.Text = "L";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.BoardButton_Click);
            this.button3.MouseEnter += new System.EventHandler(this.ChangeColorMouseEnter);
            this.button3.MouseLeave += new System.EventHandler(this.ChangeColorMouseLeave);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.Yellow;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.PapayaWhip;
            this.button2.Location = new System.Drawing.Point(86, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(78, 70);
            this.button2.TabIndex = 1;
            this.button2.Text = "E";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.BoardButton_Click);
            this.button2.MouseEnter += new System.EventHandler(this.ChangeColorMouseEnter);
            this.button2.MouseLeave += new System.EventHandler(this.ChangeColorMouseLeave);
            // 
            // connectPanel
            // 
            this.connectPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.connectPanel.Controls.Add(this.connectDisconnect);
            this.connectPanel.Controls.Add(this.label2);
            this.connectPanel.Controls.Add(this.label1);
            this.connectPanel.Controls.Add(this.nameField);
            this.connectPanel.Controls.Add(this.IPField);
            this.connectPanel.Location = new System.Drawing.Point(12, 27);
            this.connectPanel.Name = "connectPanel";
            this.connectPanel.Size = new System.Drawing.Size(230, 134);
            this.connectPanel.TabIndex = 2;
            this.connectPanel.Visible = false;
            // 
            // connectDisconnect
            // 
            this.connectDisconnect.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.connectDisconnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectDisconnect.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.connectDisconnect.Location = new System.Drawing.Point(8, 79);
            this.connectDisconnect.Name = "connectDisconnect";
            this.connectDisconnect.Size = new System.Drawing.Size(211, 45);
            this.connectDisconnect.TabIndex = 4;
            this.connectDisconnect.Text = "GO!";
            this.connectDisconnect.UseVisualStyleBackColor = false;
            this.connectDisconnect.Click += new System.EventHandler(this.connectDisconnect_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label2.Location = new System.Drawing.Point(5, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Player Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label1.Location = new System.Drawing.Point(5, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Server IP";
            // 
            // nameField
            // 
            this.nameField.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.nameField.Location = new System.Drawing.Point(87, 47);
            this.nameField.Name = "nameField";
            this.nameField.Size = new System.Drawing.Size(132, 20);
            this.nameField.TabIndex = 1;
            // 
            // IPField
            // 
            this.IPField.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.IPField.Location = new System.Drawing.Point(87, 12);
            this.IPField.Name = "IPField";
            this.IPField.Size = new System.Drawing.Size(132, 20);
            this.IPField.TabIndex = 0;
            // 
            // WordTextBox
            // 
            this.WordTextBox.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.WordTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WordTextBox.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.WordTextBox.Location = new System.Drawing.Point(16, 346);
            this.WordTextBox.Name = "WordTextBox";
            this.WordTextBox.ReadOnly = true;
            this.WordTextBox.Size = new System.Drawing.Size(252, 60);
            this.WordTextBox.TabIndex = 3;
            this.WordTextBox.Click += new System.EventHandler(this.WordTextBox_Click);
            this.WordTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.WordTextBox_KeyPress);
            // 
            // AttackButton
            // 
            this.AttackButton.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.AttackButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AttackButton.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.AttackButton.Location = new System.Drawing.Point(274, 343);
            this.AttackButton.Name = "AttackButton";
            this.AttackButton.Size = new System.Drawing.Size(80, 63);
            this.AttackButton.TabIndex = 4;
            this.AttackButton.Text = "Attack!";
            this.AttackButton.UseVisualStyleBackColor = false;
            this.AttackButton.Click += new System.EventHandler(this.AttackButton_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Info;
            this.label3.Location = new System.Drawing.Point(358, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(173, 46);
            this.label3.TabIndex = 5;
            this.label3.Text = "TIME LEFT:";
            // 
            // TimeLeftLabel
            // 
            this.TimeLeftLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeLeftLabel.ForeColor = System.Drawing.Color.DarkSeaGreen;
            this.TimeLeftLabel.Location = new System.Drawing.Point(600, 28);
            this.TimeLeftLabel.Name = "TimeLeftLabel";
            this.TimeLeftLabel.Size = new System.Drawing.Size(75, 41);
            this.TimeLeftLabel.TabIndex = 6;
            this.TimeLeftLabel.Text = "N/A";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.Info;
            this.label5.Location = new System.Drawing.Point(357, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(237, 46);
            this.label5.TabIndex = 7;
            this.label5.Text = "Your Score:";
            // 
            // OpponentNameLabel
            // 
            this.OpponentNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpponentNameLabel.ForeColor = System.Drawing.SystemColors.Info;
            this.OpponentNameLabel.Location = new System.Drawing.Point(354, 152);
            this.OpponentNameLabel.Name = "OpponentNameLabel";
            this.OpponentNameLabel.Size = new System.Drawing.Size(240, 46);
            this.OpponentNameLabel.TabIndex = 8;
            this.OpponentNameLabel.Text = "Their Score:";
            // 
            // OpponentsScoreLabel
            // 
            this.OpponentsScoreLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OpponentsScoreLabel.ForeColor = System.Drawing.Color.DarkSeaGreen;
            this.OpponentsScoreLabel.Location = new System.Drawing.Point(600, 152);
            this.OpponentsScoreLabel.Name = "OpponentsScoreLabel";
            this.OpponentsScoreLabel.Size = new System.Drawing.Size(71, 41);
            this.OpponentsScoreLabel.TabIndex = 9;
            this.OpponentsScoreLabel.Text = "N/A";
            // 
            // ClientScoreLabel
            // 
            this.ClientScoreLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClientScoreLabel.ForeColor = System.Drawing.Color.DarkSeaGreen;
            this.ClientScoreLabel.Location = new System.Drawing.Point(600, 108);
            this.ClientScoreLabel.Name = "ClientScoreLabel";
            this.ClientScoreLabel.Size = new System.Drawing.Size(72, 41);
            this.ClientScoreLabel.TabIndex = 10;
            this.ClientScoreLabel.Text = "N/A";
            // 
            // SummaryTextBox
            // 
            this.SummaryTextBox.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.SummaryTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SummaryTextBox.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.SummaryTextBox.Location = new System.Drawing.Point(358, 218);
            this.SummaryTextBox.Multiline = true;
            this.SummaryTextBox.Name = "SummaryTextBox";
            this.SummaryTextBox.ReadOnly = true;
            this.SummaryTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.SummaryTextBox.Size = new System.Drawing.Size(313, 188);
            this.SummaryTextBox.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label9.Location = new System.Drawing.Point(361, 198);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Your words so far:";
            // 
            // GameOutcomeLabel
            // 
            this.GameOutcomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GameOutcomeLabel.ForeColor = System.Drawing.Color.DarkSeaGreen;
            this.GameOutcomeLabel.Location = new System.Drawing.Point(352, 61);
            this.GameOutcomeLabel.Name = "GameOutcomeLabel";
            this.GameOutcomeLabel.Size = new System.Drawing.Size(320, 47);
            this.GameOutcomeLabel.TabIndex = 13;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(683, 418);
            this.Controls.Add(this.connectPanel);
            this.Controls.Add(this.GameOutcomeLabel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.SummaryTextBox);
            this.Controls.Add(this.ClientScoreLabel);
            this.Controls.Add(this.OpponentsScoreLabel);
            this.Controls.Add(this.OpponentNameLabel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TimeLeftLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.AttackButton);
            this.Controls.Add(this.WordTextBox);
            this.Controls.Add(this.menuStrip1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximumSize = new System.Drawing.Size(699, 456);
            this.MinimumSize = new System.Drawing.Size(699, 456);
            this.Name = "Form1";
            this.Text = "Boggle";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.connectPanel.ResumeLayout(false);
            this.connectPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem connectToolStripMenuItem;
        private System.Windows.Forms.Panel connectPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nameField;
        private System.Windows.Forms.TextBox IPField;
        private System.Windows.Forms.Button connectDisconnect;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox WordTextBox;
        private System.Windows.Forms.Button AttackButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label TimeLeftLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label OpponentNameLabel;
        private System.Windows.Forms.Label OpponentsScoreLabel;
        private System.Windows.Forms.Label ClientScoreLabel;
        private System.Windows.Forms.TextBox SummaryTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label GameOutcomeLabel;
    }
}

