﻿// AUTHORS: Scott Wells & Matthew Mendez
// Date Modified: 12/5/14

using CustomNetworking;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BoggleClient
{
    /// <summary>
    /// This Form is a representation of a BoggleClient. A BoggleClient
    /// connects to a BoggleServer and is able to play a game of Boggle
    /// with another connected BoggleClient. 
    /// </summary>
    public partial class Form1 : Form
    {
        /// <summary>
        /// The underlying socket that sends messages to the server.
        /// </summary>
        private StringSocket socket;

        /// <summary>
        /// Flag that represents whether a game has started.
        /// Certain properties of the GUI are changed when a 
        /// game is in progress.
        /// </summary>
        private bool gameStarted;

        /// <summary>
        /// Flag that represents whether a games has ended.
        /// Certain properties of the GUI are changed when a 
        /// game ends.
        /// </summary>
        private bool gameEnded;

        /// <summary>
        /// Constructor for the BoggleClient Form. The socket
        /// is not initialized at this point. The socket is
        /// initialized after the user of the BoggleClient
        /// specifies which IP address they would like to connect
        /// to.
        /// </summary>
        public Form1()
        {    
            InitializeComponent();
            gameStarted = false;
            gameEnded = false;
            WordTextBox.ReadOnly = true;
        }

        /// <summary>
        /// When the "Connect" tool strip menu item is clicked, the
        /// panel that "drops down" from it is toggled between being visible
        /// and not visible.
        /// </summary>
        public void connectToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            connectPanel.Visible = (!connectPanel.Visible);
        }

        /// <summary>
        /// The button on the connect/disconnect panel has two states: "GO!" and
        /// "DISCONNECT". When GO! is pressed, the Client is attempted to connect
        /// to the given IP address or domain name. When it's DISCONNECT state is 
        /// pressed, the user is disconnected from the ongoing game.
        /// </summary>
        public void connectDisconnect_Click(object sender, EventArgs e)
        {
            try // If there is an error connecting to the host, the user is notified.
            {
                // The panel visibility is toggled back to invisible.
                connectToolStripMenuItem.PerformClick(); 

                if (connectDisconnect.Text == "GO!")
                {
                    // Toggle the connect/disconnect button's display.
                    connectDisconnect.Text = "Give Up";

                    String ipString = IPField.Text;
                    String playerName = nameField.Text;
                    IPAddress ipAddress;
                    IPHostEntry hostEntry;
                    TcpClient underlyingClient;

                    // If the text given is a domain name, it is parsed accordingly,
                    // else it is used as is.
                    if (!IPAddress.TryParse(ipString, out ipAddress))
                    {
                        ipAddress = Dns.GetHostAddresses(ipString)[0];
                        hostEntry = Dns.GetHostEntry(ipAddress);
                        underlyingClient = new TcpClient(hostEntry.HostName, 2000);
                    }
                    else
                    {
                        hostEntry = Dns.GetHostEntry(ipAddress);
                        underlyingClient = new TcpClient(hostEntry.HostName, 2000);
                    }

                    // Shake hands with the server and send the "PLAY" protocol
                    // associated with the server, and begin listening for a response
                    // from the server.
                    socket = new StringSocket(underlyingClient.Client, UTF8Encoding.Default);
                    socket.BeginSend("PLAY " + playerName + "\n", (ex, o) => { if (ex != null) { ResetClient(true); } }, null);
                    socket.BeginReceive(StartCallback, null);
                    
                    // Update the view.
                    GameOutcomeLabel.Text = "";
                    SummaryTextBox.Text = "Waiting for opponent.\r\n";
                    SetValue(ClientScoreLabel, "0");
                    SetValue(OpponentsScoreLabel, "0");
                }
                else // Passing null to reset client notifies the user that they have given up. 
                    ResetClient(null);
            }
            catch (Exception) { SummaryTextBox.Text = "Could not connect to server. Check the host IP."; connectDisconnect.Text = "GO!";};
        }
        /// <summary>
        /// 
        /// </summary>
        private void StartCallback(String startString, Exception e, Object payload)
        {
            if (e != null) 
                ResetClient(true); // Passing true means the server crashed.
            else if(startString == "TERMINATED")
                ResetClient(false); // Passing false means the other client stopped the game.
            else if (startString.StartsWith("START "))
            {
                // Update the view
                SetValue(ClientScoreLabel, "0");
                SetValue(OpponentsScoreLabel, "0");
                SetValue(SummaryTextBox, "Game On.\r\n");

                // Follow protocol to split the string into parts.
                string[] arr = startString.Split(null);
                string board = arr[1];
                string time = arr[2];
                string name = "";
                // The name can be multiple words long.
                for (int i = 3; i < arr.Length; i++)
                    name += arr[i] + " ";
                name = name.Substring(0, name.Length - 1);

                // The gameboard is populated along with the inital
                // time of the game and the opponent's name.
                PopulateClientView(board, time, name);

                gameStarted = true;
                gameEnded = false;

                // Begin listening for TIME protocol
                socket.BeginReceive(GameEventsCallback, null);
            }   
        }

        /// <summary>
        /// This callback handles the receiving of TERMIATED, TIME, SCORE, and
        /// STOP protocol and parses them accordingly.
        /// </summary>
        private void GameEventsCallback(string eventString, Exception e, Object payload)
        {
            if (e != null)
            {
                if(!gameEnded)
                    ResetClient(true); // The server crashed.
                return;
            }
            // The following is necessary due to a problem with our server,
            // all other opponent terminations are handled below.
            else if (eventString == null)
            {
                ResetClient(false);  // The other opponent disconnected.
                return;
            }

            string[] arr = eventString.Split(null);

            // The other client disconnected.
            if (eventString == "TERMINATED")
                ResetClient(false);

            // The remaining game time has been received.
            else if (eventString.StartsWith("TIME "))
            {
                SetValue(TimeLeftLabel, arr[1]);
            }

            // The scores of each player have been received.
            else if (eventString.StartsWith("SCORE "))
            {
                SetValue(ClientScoreLabel, arr[1]);
                SetValue(OpponentsScoreLabel, arr[2]);
            }

            // The game is over.
            else if (eventString.StartsWith("STOP "))
            {
                gameEnded = true;

                DisplayGameSummary(arr);

                // Set the game outcome label appropriately.
                if ((Int32.Parse(ClientScoreLabel.Text) > Int32.Parse(OpponentsScoreLabel.Text)))
                    SetValue(GameOutcomeLabel, "YOU WINNER!");
                else if ((Int32.Parse(ClientScoreLabel.Text) < Int32.Parse(OpponentsScoreLabel.Text)))
                    SetValue(GameOutcomeLabel, "YOU LOSER!");
                else
                    SetValue(GameOutcomeLabel, "STALEMATE");

                // Toggle the connect/disconnect button
                SetValue(connectDisconnect, "GO!");
                return; // Do not continue listening for messages.
            }
            socket.BeginReceive(GameEventsCallback, null);        
        }

        /// <summary>
        /// Used to assist the InvokeRequired/Invoke process in the
        /// SetValue function.
        /// </summary>
        delegate void valueDelegate(Control component, string value);

        /// <summary>
        /// C# does not allow the GUI to be changed from threads other
        /// than the GUI thread without checking if an invoke is required
        /// and invoking it if one is. This method is used to set the text
        /// value of any control component of the gui including text boxes,
        /// labels, and buttons.
        /// </summary>
        private void SetValue(Control control, string value)
        {
            if (control.InvokeRequired)
            {
                control.Invoke(new valueDelegate(SetValue), control, value);
            }
            else
            {
                control.Text = value;
            }
        }

        /// <summary>
        /// Sets all fields sent from the "START" protocol from the server. This includes
        /// the text value of all buttons on the board, the length of time that the game
        /// is to be, and the opponents name.
        /// </summary>
        private void PopulateClientView(string board, string time, string opponentsName)
        {
            Button[] list = {button1, button2, button3, button4, button5, button6, button7, button8, button9, button10, button11, button12, button13, button14, button15, button16};
            
            for (int i = 0; i < 16; i++)
            {
                 SetValue(list[i], board[i].ToString());
            }

            SetValue(TimeLeftLabel, time);

            SetValue(OpponentNameLabel, opponentsName + "'s Score:");
        }

        /// <summary>
        /// Parses the game summary sent from the server using the "STOP" 
        /// protocol into easily readable and navigateable format. The summary
        /// is then printed to the summary text box.
        /// </summary>
        private void DisplayGameSummary(string[] arr)
        {
            // Note: arr[0] is the "STOP" keyword and is ignored.

            // Print the number of legal words this client played followed
            // by the list of words.
            String textBoxText = "You played " + arr[1] + " legal words: ";
            int i = 2;
            int b;
            while (!Int32.TryParse(arr[i], out b))
            {
                textBoxText += arr[i++] + " ";    
            }

            // Print the number of legal words the opponent played followed
            // by the list of words.
            textBoxText += "\r\nThey played " + arr[i++] + " legal words: ";
            while (!Int32.TryParse(arr[i], out b))
                textBoxText += arr[i++] + " ";

            // Print the number of common words played and the list of words.
            textBoxText += "\r\nThere were " + arr[i++] + " common words: ";
            while (!Int32.TryParse(arr[i], out b))
                textBoxText += arr[i++] + " ";

            // Print the number of illegal words this client played followed
            // by the list of words.
            textBoxText += "\r\nYou played " + arr[i++] + " illegal words: ";
            while (!Int32.TryParse(arr[i], out b))
                textBoxText += arr[i++] + " ";

            // Print the number of illegal words the opponent played followed
            // by the list of words.
            textBoxText += "\r\nThey played " + arr[i++] + " illegal words: ";
            while(i < arr.Length)
                textBoxText += arr[i++] + " ";
            SetValue(SummaryTextBox, textBoxText);
        }

        /// <summary>
        /// ResetClient shows a message explaining why the GUI is being reset
        /// and makes the GUI look as it did when it was opened. If null is passed,
        /// it means the user stopped the game. If true is passed, it means the server
        /// crashed. If false is passed, it means the opponent stopped the game.
        /// </summary>
        private void ResetClient(bool? serverCrashed)
        {
            gameEnded = true;

            socket.Close();

            if (serverCrashed == null)
                MessageBox.Show("You have given up. At least you tried.");
            else if (serverCrashed.Value)
                MessageBox.Show("The server crashed.");
            else
                MessageBox.Show("Your opponent gave up. Good job.");

            // Reset the view.
            SetValue(connectDisconnect, "GO!");
            SetValue(TimeLeftLabel, "N/A");
            string board = "WELCOMETOBOGGLE!"; // The string to be printed on the game board.
            Button[] list = {button1, button2, button3, button4, button5, button6, button7, button8, button9, button10, button11, button12, button13, button14, button15, button16};
            for (int i = 0; i < 16; i++)
                 SetValue(list[i], board[i].ToString());
            SetValue(ClientScoreLabel, "0");
            SetValue(OpponentsScoreLabel, "0");
            SetValue(SummaryTextBox, "");
        }

        /// <summary>
        /// When the attack button is clicked, the text in the text box is
        /// sent to the server as a played word and the text is shown in the
        /// "words played so far" box. 
        /// </summary>
        public void AttackButton_Click(object sender, EventArgs e)
        {
            // The game must be in progress for the attack button to do anything.
            if (gameStarted && !gameEnded)
            {
                socket.BeginSend("WORD " + WordTextBox.Text + "\n", (ex, o) => {}, null);
                SetValue(SummaryTextBox, SummaryTextBox.Text + WordTextBox.Text + "\r\n");

                // Reset the word text box so the user can type another word.
                SetValue(WordTextBox, "");
            }
        }

        /// <summary>
        /// The text box is activated to type in when clicked if there is 
        /// a game ongoing.
        /// </summary>
        public void WordTextBox_Click(object sender, EventArgs e)
        {
            if (gameStarted)
                WordTextBox.ReadOnly = false;
        }

        /// <summary>
        /// Handles all keyed keys when the WordTextBox is in focus. The enter
        /// key has the same functionality as clicking attack and submits a word.
        /// 
        /// </summary>
        public void WordTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            // NOTE: The e.handled functionality keeps the keys from making a 
            // dinging error noise on windows when they are pressed. It also 
            // marks them as "handled". If handled is false, nothing happens
            // when the key is pressed.

            if (!gameEnded)
            {
                // The enter key has the same functionality as clicking attack.
                if (e.KeyChar == (char)Keys.Return)
                {
                    e.Handled = true;
                    if (WordTextBox.Text != "")
                    {
                        AttackButton_Click(null, null);
                        WordTextBox.Text = "";
                    }
                }
                // if the text input is not valid input, handled will stop the char from being entered into the text box.
                else if (!System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), @"[A-Z]|[a-z]|[\b]"))
                    e.Handled = true;
                else
                    e.Handled = false;
            }
        }

        /**
        * Boggle Board Buttons Enter text into the Text enter field
        **/

        /// <summary>
        /// When the buttons on the boggle board are clicked, the value
        /// of the character on the button is appended to the text in 
        /// text box where words are typed. This allows the user to
        /// interactively create words.
        /// </summary>
        public void BoardButton_Click(object sender, EventArgs e)
        {
            if (!gameEnded)
            {
                Button button = (Button)sender;
                if (gameStarted)
                    SetValue(WordTextBox, WordTextBox.Text += button.Text);
                WordTextBox.Focus();
            }
        }

        /// <summary>
        /// When the boggle board buttons are moused over, they are
        /// "highlighted" to the programs secondary lighter blue color.
        /// </summary>
        public void ChangeColorMouseEnter(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.UseVisualStyleBackColor = false;
            button.BackColor = Color.DarkSeaGreen;
            button.ForeColor = Color.DarkSlateGray;
        }

        /// <summary>
        /// When the boggle board buttons are moused off (after being
        /// moused over) they return to the primary color of the boggle
        /// GUI (darker blue).
        /// </summary>
        public void ChangeColorMouseLeave(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.BackColor = Color.DarkSlateGray;
            button.ForeColor = Color.PapayaWhip;
        }

        /**
        * Used for testing only.
        **/

        /// <summary>
        /// Used for testing purposes. Allows the test class
        /// to access and set the text value of the IPfield
        /// on the connect/disconnect panel.
        /// </summary>
        public void setIpFieldValue(String s)
        {
            SetValue(IPField, s);
        }

        /// <summary>
        /// Used for testing purposes. Allows the test class
        /// to access and set the text value of the text box
        /// that words are tyed into to help simulate a client
        /// playing a game.
        /// </summary>
        public void setWordBoxTextValue(String s)
        {
            SetValue(WordTextBox, s);
        }

        /// <summary>
        /// Used for testing purposes. Allows the test class
        /// to get the text value of the summary text box as
        /// the game progesses.
        /// </summary>
        public string getSummaryTextBoxText()
        {
            return SummaryTextBox.Text;
        }
    }
}
