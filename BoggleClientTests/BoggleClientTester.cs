﻿// AUTHORS: Scott Wells & Matthew Mendez
// Date Modified: 12/5/14

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace BoggleClientTestProject
{
    /// <summary>
    /// Tests the BoggleClient. There is one large test that tests and asserts
    /// every case in the program so that there are not issues with the clients
    /// and ports interfering with each other. The tests do not always work the 
    /// first time through (probably due to the instructions not being cached
    /// and slowing down the program so that the Thread.Sleep() methods due not
    /// work correctly). Also, the MessageBoxes at the very end of the test must be
    /// exited out of for the test to complete. 
    /// </summary>
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void GameSimulationTest()
        {
            // SETUP SERVER AND CLIENTS
            BoggleClient.Form1 player1 = new BoggleClient.Form1();
            BoggleClient.Form1 player2 = new BoggleClient.Form1();
            Process server = new Process();
            server.StartInfo.FileName = @"..\..\BoggleServer.exe";
            server.StartInfo.Arguments = @"15 ..\..\dictionary.txt RESTARUANTSHYPPA";
            server.Start();

            // PART 1: Test that the BoggleClient gracefully displays a message when 
            // an invalid host name is entered.
            player1.setIpFieldValue("pfpfpfpfppfpfpfpfpfppfpfpfpfpfpfpfppfpfpfpfpfpfpfpfppfpf");
            player1.connectDisconnect_Click(null, null); // Attempts to connect
            Thread.Sleep(5000);
            Assert.AreEqual("Could not connect to server. Check the host IP.", player1.getSummaryTextBoxText());

            // PART 2: 
            player1.setIpFieldValue("localhost");
            player1.connectDisconnect_Click(null, null);
            player2.setIpFieldValue("localhost");
            player2.connectDisconnect_Click(null, null);
            Thread.Sleep(5000);
            Assert.AreEqual("Game On.\r\n", player1.getSummaryTextBoxText());
            Assert.AreEqual("Game On.\r\n", player2.getSummaryTextBoxText());
            player2.WordTextBox_Click(null, null);
            // Begin to play words from both players.
            player1.setWordBoxTextValue("resTaUrantS");
            player1.WordTextBox_KeyPress(null, new KeyPressEventArgs((char)Keys.Return));
            player1.WordTextBox_KeyPress(null, new KeyPressEventArgs((char)Keys.Return)); // Attempt playing an empty string.
            player2.setWordBoxTextValue("happy");
            player2.AttackButton_Click(null, null);
            player2.setWordBoxTextValue("sure");
            player2.AttackButton_Click(null, null);
            player1.setWordBoxTextValue("sure");
            player1.AttackButton_Click(null, null);
            player1.setWordBoxTextValue("MARBLES");
            player1.AttackButton_Click(null, null);
            player2.setWordBoxTextValue("FFFFFFFFHI");
            player2.AttackButton_Click(null, null);
            Thread.Sleep(15000); // Sleeps so that the game is ended before the assertion is made.
            // Assert that the game summary is correctly displayed.
            Assert.AreEqual("You played 1 legal words: RESTAURANTS \r\nThey played 1 legal words: HAPPY \r\nThere were 1 common words: SURE \r\nYou played 1 illegal words: MARBLES \r\nThey played 1 illegal words: FFFFFFFFHI ", player1.getSummaryTextBoxText());

            // Tests Stalemate: If both players don't play anything, they will tie.
            player1.connectDisconnect_Click(null, null);
            player2.connectDisconnect_Click(null, null);
            Thread.Sleep(15000);

            // PART 4: tests reset gui method
            Thread.Sleep(10000);
            player1.connectDisconnect_Click(null, null);
            player2.connectDisconnect_Click(null, null);
            player2.connectDisconnect_Click(null, null);
            Thread.Sleep(5000);
            
            // PART 5: Tests the button clicking for the Boggle board.
            player1.BoardButton_Click(new Button(), null);
            player1.ChangeColorMouseEnter(new Button(), null);
            player1.ChangeColorMouseLeave(new Button(), null);
        }
    }
}
